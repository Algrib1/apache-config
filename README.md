# apache-conf

Create Jinja2 template for Apache configuration from lesson.

Create _vhosts.j2_, _data.yml_, _conf.py_ according to the instructions from the lesson.

Your solution should contain  the following files:
- _vhosts.j2_
- _data.yml_
- _conf.py_
- generated _vhosts.conf_ 
